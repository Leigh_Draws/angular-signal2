import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { BlocInfoComponent } from './bloc-info/bloc-info.component';
import { HeaderComponent } from './header/header.component';
import { HeroComponent } from './hero/hero.component';
import { Infobloc } from './bloc-info/infobloc';
import { NgForOf } from '@angular/common';
import { lastInfo } from './last-info-type';
import { LastInformationComponent } from './last-information/last-information.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, BlocInfoComponent, HeaderComponent, NgForOf, HeroComponent, LastInformationComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  
  // Création de la liste de bloc d'information avec l'interface InfoBloc
  informationBlocList: Infobloc[] = [
    {
      id: 1,
      title: 'Dites ce qui vous plaît',
      paragraph:
        'Partagez gratuitement des textes, des messages vocaux, des photos, des vidéos et des GIF. Signal utilise la connexion de données de votre téléphone afin de vous éviter les frais relatifs à l’envoi de texto et de message multimédia.',
      image: 'https://signal.org/assets/images/features/Media.png',
    },
    {
      id: 2,
      title: 'Exprimez-vous librement',
      paragraph:
        'Passez des appels vocaux ou vidéo clairs avec des personnes qui vivent à l’autre bout de la ville ou du monde, sans frais supplémentaires',
      image: 'https://signal.org/assets/images/features/Calls.png',
    },
    {
      id: 3,
      title: 'Respectez votre vie privée',
      paragraph:
        'Avec nos stickers chiffrés, exprimez-vous encore plus librement. Vous êtes même libre de créer et de partager vos propres packs de stickers.',
      image: 'https://signal.org/assets/images/features/Stickers.png',
    },
    {
      id: 4,
      title: 'Rassemblez-vous avec les groupes',
      paragraph:
        'Avec les conversations de groupe, restez facilement en contact avec votre famille, vos amis et vos collègues.',
      image: 'https://signal.org/assets/images/features/Groups.png',
    }
  ];

  lastInformation: lastInfo[] = [
    {
      title: 'Aucune publicité, aucun traqueur, vraiment.',
      paragraph: 'Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni aucun système de suivi inquiétant. Vous pouvez vous consacrer à partager les moments importants avec les personnes qui comptent pour vous.',
      image: 'https://signal.org/assets/images/features/No-Ads.png'
    },
    {
      title: 'Gratuit pour tous',
      paragraph: 'Signal est un organisme à but non lucratif indépendant. Nous ne sommes reliés à aucune entreprise technologique importante et nous ne pourrons jamais être achetés par l’une d’elles. Le développement de notre plateforme est financé par des subventions et des dons de personnes comme vous. ',
      image: 'https://signal.org/assets/images/features/Nonprofit503.png'
    }
  ]
  
}
