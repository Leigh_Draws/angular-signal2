import { Component, Input } from '@angular/core';
import { Infobloc } from './infobloc';

@Component({
  selector: 'app-bloc-info',
  standalone: true,
  imports: [],
  templateUrl: './bloc-info.component.html',
  styleUrl: './bloc-info.component.css',
})
export class BlocInfoComponent {
  // Input pour faire passer les informations de chaque bloc d'information individuels
  @Input() informationBloc!: Infobloc;

}
