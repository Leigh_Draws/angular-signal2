export interface Infobloc {
    id: number;
    title : string;
    paragraph : string;
    image : string;
}
