import { Component, Input } from '@angular/core';
import { lastInfo } from '../last-info-type';

@Component({
  selector: 'app-last-information',
  standalone: true,
  imports: [],
  templateUrl: './last-information.component.html',
  styleUrl: './last-information.component.css'
})
export class LastInformationComponent {
  @Input() eachLastInformation!: lastInfo;
}
